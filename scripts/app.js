// angular  .module("ngClassifieds",[]);

angular
  .module("ngcontacts",["ngMaterial"])
  .config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('cyan')
      .accentPalette('orange');
  });
