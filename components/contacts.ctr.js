(function () {
  "use strict";
  angular
  .module("ngcontacts",['ngMaterial', 'ngMessages'])
  .controller("contactsCtrl",function ( $scope , $http , contactsFactory , $mdSidenav ,$mdToast , $mdDialog) {
    contactsFactory.getcontacts().then( function (contacts) {
      $scope.contacts = _.sortBy(contacts.data,'firstname');
      $scope.contactsAll = $scope.contacts;
      $scope.selectedContact = $scope.contacts[0];
      $scope.alphahLinks = [];
      $scope.getFirstChar($scope.contacts);
    });

    $scope.getFirstChar = function (contacts) {
      $scope.alphahLinks=[];
      for (var i = 0; i < contacts.length; i++) {
        $scope.alphahLinks.push( contacts[i].firstname.charAt(0).toUpperCase() );
      }
      $scope.alphahLinks=_.uniq($scope.alphahLinks);
    };

    $scope.openSidebar = function () {
      $mdSidenav('left').open();
    };
    $scope.closeSidebar = function () {
      $mdSidenav('left').close();
    };

    // $scope.updatejsonFile = function( obj ){
    //   var file = data/contacts2.json;
    //   jsonfile.writeFile(file, obj, function (err) {
    //     console.error(err);
    //   });
    // };

    $scope.saveContact = function (contact) {
      if (contact) {
        $scope.contacts=$scope.contactsAll;

        contact.id = $scope.contacts.length + 1;
        $scope.contacts.push(contact);
        $scope.contacts = _.sortBy($scope.contacts,'firstname');
        $scope.contactsAll = $scope.contacts;
        $scope.contact= {};
        $scope.closeSidebar();
        $scope.getFirstChar($scope.contacts);
        // $scope.updatejsonFile(contact);
        $scope.showToast("contact Saved");
        console.log($scope.contactsAll);
      }
    };

    $scope.applyContactFilter = function (letter){
      // $scope.contactFilter = letter;

      function getAllIndexes(arr, val) {
        var indexes = [], i = -1;
        while ((i = arr.indexOf(val, i+1)) != -1){
          indexes.push(i);
        }
        return indexes;
      }

      if (letter=='ALL') {
        $scope.contacts = $scope.contactsAll;
      }
      else {
        var names = [];
        var namesFirstLetter = [];
        for (var i = 0; i < $scope.contactsAll.length; i++) {
          names.push( $scope.contactsAll[i].firstname );
        }

        for (var j = 0; j < names.length; j++) {
          namesFirstLetter.push( names[j].charAt(0).toUpperCase() );
        }

        var indexes = getAllIndexes(namesFirstLetter, letter);
        $scope.contacts = [];

        for (var k = 0; k < indexes.length; k++) {
          $scope.contacts.push(  $scope.contactsAll[indexes[k]] );
        }
      }
    };

    $scope.showToast = function ( message ) {
      $mdToast.show(
        $mdToast.simple()
          .content( message )
          .position('top, right')
          .hideDelay(3000)
      );
    };

    $scope.editing=false;



    $scope.editContact = function () {
      $scope.editing = true;
      $scope.openSidebar();
      $scope.contact = $scope.selectedContact;
      // console.log($scope.selectedContact);
    };

    $scope.saveEdit = function (contact) {

      //Update contact contacts
      $scope.contactsById = _.sortBy($scope.contacts,"id");
      $scope.contactsById[parseInt(contact.id)-1] = contact;
      $scope.contacts = _.sortBy($scope.contactsById,"firstname");
      $scope.contactsAll = $scope.contacts;
      $scope.getFirstChar($scope.contacts);
      $scope.editing = false;
      $scope.closeSidebar();
      $scope.contact = {};
      $scope.showToast("Edit Saved");
    };

    $scope.showDescription = function (contact) {
      $scope.selectedContact = contact;
    };

  });
})();
