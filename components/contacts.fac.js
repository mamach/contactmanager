(function () {
  "use strict";

  angular
  .module("ngcontacts")
  .factory("contactsFactory",function ($http) {

    function getcontacts() {
      return $http.get('data/contacts.json');
    }

    return{
      getcontacts:getcontacts
    };
  });
})();
